package org.sakaiproject.tool.assessment.entity.api;

import org.sakaiproject.entitybroker.entityprovider.EntityProvider;

/**
 * Entity Provider for samigo PublishedAssessments
 * 
 * @author Charles Hedrick hedrick@rutgers.edu
 *
 */
public class CoreAssessmentEntityProvider {

  public final static String ENTITY_PREFIX = "sam_core";

}
